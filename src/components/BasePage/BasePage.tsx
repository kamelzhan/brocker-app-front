import { BasePageProps } from "./props";
import { Filter } from "../Filter/Filter";
import { Divider, Typography } from "@mui/material";
import Grid from "../UI/Grid/Grid";
import { PrimaryButton } from "../UI/Button/PrimaryButton";
import "./BasePage.scss";
import { DocumentMenu } from "./components/DocumentMenu";
import { Link } from "react-router-dom";

export const BasePage = <R,>({buttonsTitle, title, fetchRows, columns, inputs, headerLabel, isFilterActive = true, path="", actions = {edit:true, view: true, delete: true}}: BasePageProps<R>) => {
    
    return(
        <div className="base-page-container">
            <div className="base-page-header">
                <Typography fontSize={"36px"} color={"#1C3557"}>
                    {title}
                </Typography>
                {buttonsTitle.map((button, index) => {
                    return (
                        <div className="base-page-header-btn" key={`header-btn-${index}`}>
                            <PrimaryButton variant="outlined" size="small">
                                {button.link
                                    ? <Link className="Link" to={`/${path}/create`}> {button.name} </Link>
                                    : button.name
                                }
                            </PrimaryButton>
                        </div>
                    );
                })}
            </div>

            {isFilterActive && 
                <div>
                    <Divider/>

                    <div className="base-page-filter">
                        <Filter inputs={inputs} headerLabel={headerLabel}/>
                    </div>
                </div>
            }
            
            <Divider/>
            <Grid 
                columns={[
                    ...columns,
                    {
                        label:'',
                        render: (doc) => <DocumentMenu actions={actions} doc={doc}/>
                    }
                ]} 
                fetchRows={fetchRows}/>
        </div>
    )
}