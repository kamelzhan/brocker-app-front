import { Paper, Tab, Table, TableCell, TableContainer, TableRow, Typography } from "@mui/material"
import { BasePageDetailsProps } from "./props"
import { PrimaryButton } from "../../UI/Button/PrimaryButton"
import { PrimaryInput } from "../../UI/Input/PrimaryInput";
import { FC, useEffect, useState } from "react";
import "./BasePageDetails.scss";
import { Link, useParams } from "react-router-dom";
import axios from "axios";

export const BasePageDetails:FC<BasePageDetailsProps> = ({data, path}) => {

    let {isEdit, id} = useParams();
    const isEditable = isEdit === "edit" ? true : false;
    const [responseBody, setResponseBody] = useState({});
    useEffect(()=>{
        if (isEditable){
            axios.get(`/ets/${path}/${id}`)
            .then((res)=>{
                setResponseBody(res.data);
            })
        }
    },[])
    

    const handleInputChange = (value: string, name: string) => {
        setResponseBody({...responseBody, [name]: value});
    }

    const handleClick = () => {
        console.log(responseBody);
    }
    return (
        <div className="details-container">
            {/* {Object.keys(data).map((key)=>{
                return(
                    <div>
                        <h1>{}</h1>
                    </div>
                )
            })} */}
            <h1>{data.generalPart.title}</h1>
                {data.generalPart.rows.map((row)=>{
                    return(
                        <div className="details-body">
                            <div className="details-row">
                                <div className="details-row-name">
                                    {row.columnLabel}:
                                </div>
                                <div className="details-row-value">
                                    {isEditable
                                        ? <PrimaryInput value={row.columnValue} name={row.columnName} onChange={handleInputChange}/>
                                        : row.columnValue
                                    }
                                </div>
                            </div>
                        </div>
                    )
                })}
                <div className="details-btns">
                    <div className="details-btn">
                        <PrimaryButton variant="text" >
                            <Link className="Link" to={`/${path}`}><Typography className="back-button">{isEditable ? "Отмена" : "Назад"}</Typography></Link>
                        </PrimaryButton>
                    </div>
                    {isEditable
                        ? <div className="details-btn">
                                <PrimaryButton onClick={handleClick} variant="text">
                                    Сохранить
                                </PrimaryButton>
                            </div>
                        : null
                    }
                </div>
        </div>
    )
}