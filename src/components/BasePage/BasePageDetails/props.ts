export interface BasePageDetailsProps{
    data: DetailsRows;
    path: string;
}

export interface DetailsRows{
    generalPart: ContainerInfo;
    additionalPart?: ContainerInfo;
    additionalPart2?: ContainerInfo;
}

export interface ContainerInfo{
    title: string;
    rows: Details[];
}

export interface Details {
    columnLabel: string;
    columnValue: string;
    columnName: string;
    editable?: boolean;
}