import { ActionModel } from "../props";

export interface DocumentMenuProps{
    doc: any;
    actions: ActionModel;
}