import { FC, useState } from "react";
import { DocumentMenuProps } from "./props";
import { IconButton, Menu, MenuItem } from "@mui/material";
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Link } from "react-router-dom";


export const DocumentMenu:FC<DocumentMenuProps> = ({doc, actions}) => {
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
        const [openMenu, setOpenMenu] = useState(false);
        const handleClick = (event: React.MouseEvent<HTMLElement>) => {
            setAnchorEl(event.currentTarget);
            setOpenMenu(true);
        }
    
        const handleClose = () => {
            setAnchorEl(null);
            setOpenMenu(false);
        }
        
        return(
            <div>
                <IconButton 
                    onClick={handleClick}
                    id="demo-positioned-button"
                    aria-controls={openMenu ? 'demo-positioned-menu' : undefined}
                    aria-haspopup="true"
                    aria-expanded={openMenu ? 'true' : undefined}
                >
                    <MoreVertIcon/>
                </IconButton>
                <Menu
                    id="demo-positioned-menu"
                    open={openMenu}
                    onClose={handleClose}
                    anchorEl={anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                    aria-labelledby="demo-positioned-button"
                >
                    {actions.edit && <MenuItem><Link className="Link" to = {`${doc.id}/edit`}>Изменить</Link></MenuItem>}
                    {actions.view && <MenuItem><Link className="Link" to ={`${doc.id}/view`}>Просмотр деталей</Link></MenuItem>}
                    {actions.delete && <MenuItem>Удалить</MenuItem>}
                </Menu>
            </div>
        );
}