import { FC, useState } from "react";
import { BasePageCreateProps } from "./BasePageCreateProps";
import { Paper, Table, TableCell, TableContainer, TableRow, Typography } from "@mui/material";
import { PrimaryInput } from "../../UI/Input/PrimaryInput";
import { PrimaryButton } from "../../UI/Button/PrimaryButton";
import { Link } from "react-router-dom";
import "./BasePageCreate.scss";

export const BasePageCreate:FC<BasePageCreateProps> = ({rows, path}) => {

    const [state, setState] = useState({});
    const handleSaveButton = () => {
        console.log(state);
    }

    const handleChangeInput = (value: string, name: string) => {
        setState({...state, [name]:value});
    }

    return(
        <div className="create-container">
                {rows.map((row)=>{
                    return(
                        <div className="create-body">
                            <div className="create-row">
                                <div className="create-row-name">
                                    {row.label}
                                </div>
                                <div className="create-row-value">
                                    {
                                        row.inputType === "autocomplete" || row.inputType === "select"
                                        ? <PrimaryInput placeholder={row.label} options={row.options} type={row.inputType} name={row.name} onChange={handleChangeInput}/>
                                        : <PrimaryInput type={row.inputType} name={row.name} onChange={handleChangeInput}/>
                                    }
                                </div>
                            </div>
                        </div>
                    )
                })}
                <div className="create-btns">
                    <div className="create-btn">
                        <PrimaryButton variant="text" >
                            <Link className="Link" to={`/${path}`}><Typography className="back-button"> Отмена </Typography></Link>
                        </PrimaryButton>
                    </div>
                     <div className="create-btn">
                                <PrimaryButton onClick={handleSaveButton} variant="text">
                                    Сохранить
                                </PrimaryButton>
                    </div>
                </div>
        </div>
    );
}