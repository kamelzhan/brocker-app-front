export interface BasePageCreateProps{
    rows: RowsDetails[];
    path: string;
}

export interface RowsDetails{
    label: string;
    name: string;
    inputType: string;
    options? : string[];
}