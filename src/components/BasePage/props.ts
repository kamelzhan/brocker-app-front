import { AxiosResponse } from "axios";
import { InputCharacteristic } from "../Filter/props";
import {Columns} from "./../UI/Grid/props";

export interface BasePageProps<R>{
    title: string;
    buttonsTitle: BasePageButtons[];
    fetchRows(): Promise<AxiosResponse<R[]>> ;
    columns: Columns<R>[];
    inputs?: InputCharacteristic[];
    headerLabel?: string;
    isFilterActive?: boolean;
    path?: string;
    actions?: ActionModel;
}

export interface ActionModel{
    view: boolean;
    edit: boolean;
    delete: boolean;
}

interface BasePageButtons{
    name: string;
    onClick?: () => void;
    link?: boolean;
}