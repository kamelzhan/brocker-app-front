import {FC, useState} from "react";
import { FilterProps, InputCharacteristic } from "./props";
import { PrimaryInput } from "../UI/Input/PrimaryInput";
import { IconButton, Typography } from "@mui/material";
import { PrimaryButton } from "../UI/Button/PrimaryButton";
import FilterListIcon from '@mui/icons-material/FilterList';
import "./Filter.scss";


export const Filter: FC<FilterProps> = ({showDownloadButton = true, InputLabels, headerLabel, inputs = [] }) => {

    const [openFilter, setOpenFilter] = useState(false);

    const showFilter = () => {
        setOpenFilter(!openFilter)
    }

    return (
        <div className="filterContainer">
            <div className="filterHeader">
                <Typography fontSize={'26px'}>
                    Фильтр {headerLabel}
                </Typography>
                <IconButton onClick={showFilter}>
                    <FilterListIcon fontSize="large"/>
                </IconButton>
                
            </div>
            {openFilter && 
                <div>
                    <div className="filterBody">
                        {inputs?.map((item: InputCharacteristic, index)=>{
                            return(
                                <div className={`filter-input-${index} filter-inputs`}>
                                    <Typography className="label">
                                        {item.label}
                                    </Typography>
                                    <PrimaryInput type={item.type} size="small" className="filter-input" variant="filled" options={item.options}/>
                                </div>
                            )
                        })}
                    </div>
                    <div className="filterButtons">
                        {inputs && <div className = "filter-button">
                            <PrimaryButton size="small">
                                Поиск
                            </PrimaryButton>
                        </div>}
                        <div className = "filter-button">
                            {showDownloadButton && <PrimaryButton variant = "outlined" size = "small">
                                Скачать таблицу
                            </PrimaryButton>}
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}