export interface FilterProps{
    headerLabel?: string;
    InputLabels?: string[];
    showDownloadButton?: boolean;
    inputs?: InputCharacteristic[];
}

export interface InputCharacteristic{
    type: string;
    label: string;
    options?: string[];
}