import { FC } from "react";
import { HeaderProps } from "./props";
import { AppBar, Drawer,  IconButton, Toolbar, Typography } from "@mui/material";
import "./Header.scss";
import { Link } from "react-router-dom";
import { PrimaryButton } from "../UI/Button/PrimaryButton";
import { setCookie } from "../../service/cookies/cookies";

export const Header:FC<HeaderProps> = ({setIsAuth}) => {

    const logout = () => {
        setCookie("Token", "", {"max-age":0});
        setIsAuth();
    }

    return(
        <div className="header">
            <AppBar>
                <Toolbar className="nav-menu-items" style={{backgroundColor:"#1C3557"}}>
                    <Link className={"Link"} to={"/requests"}>
                        <Typography className = "nav-menu-item">
                            Заявки
                        </Typography>
                    </Link>
                    <Link className={"Link"} to={"/auctions"}>
                        <Typography className = "nav-menu-item">
                            Биржа
                        </Typography>
                    </Link>
                    <Link className={"Link"} to={"/deals"}>
                        <Typography className = "nav-menu-item">
                            Сделки
                        </Typography>
                    </Link>
                    <Link className={"Link"} to={"/clients"}>
                        <Typography className = "nav-menu-item">
                            Клиенты
                        </Typography>
                    </Link>
                    <Link className={"Link"} to={"/accounts"}>
                        <Typography className = "nav-menu-item">
                            Счета
                        </Typography>
                    </Link>
                    <Link className={"Link"} to={"/banks"}>
                        <Typography className = "nav-menu-item">
                            Банки
                        </Typography>
                    </Link>
                    <Link className={"Link"} to={"/customers"}>
                        <Typography className = "nav-menu-item">
                            Заказчики
                        </Typography>
                    </Link>
                    <PrimaryButton onClick={logout} variant = "text">
                        Выйти
                    </PrimaryButton>
                </Toolbar>
            </AppBar>

        </div>
    );
}