import { useEffect, useState } from "react";
import { BasePage } from "../../BasePage/BasePage";
import { AuctionsModel } from "./Models/AuctionsModel";
import axios from "axios";
import { IconButton, Menu, MenuItem } from "@mui/material";
import { Link } from "react-router-dom";
import MoreVertIcon from '@mui/icons-material/MoreVert';

export const Auctions = () => {
    

    const fetchRows = () => {
        // let result;
        // axios.get("ets/auctions")
        // .then((res) => {
        //     result.rows = res.data;
        //     result.count = res.data.length;
        // })
        // .catch((error)=>{console.log(error)})
        // console.log(result);
        // return result;
        return axios.get<AuctionsModel[]>("ets/auctions");
    }

    const calcCol = () => {
        return [
            {
                label: "ID",
                render: (doc: AuctionsModel) => doc.id
            },
            {
                label: "Код лота",
                render: (doc: AuctionsModel) => doc.lot_code
            },
            {
                label: "Название товара",
                render: (doc: AuctionsModel) => doc.product_name
            },
            {
                label: "Заказчик",
                render: (doc: AuctionsModel) => doc.customer.name
            },
            {
                label: "Стартовая цена",
                render: (doc: AuctionsModel) => doc.starting_price
            },
            {
                label: "Номер заявки",
                render: (doc: AuctionsModel) => doc.request_number
            },
            {
                label: "Подача заявок до",
                render: (doc: AuctionsModel) => doc.applying_date
            },
            {
                label: "Дата проведения",
                render: (doc: AuctionsModel) => doc.trade_date
            },
            {
                label: "Дата создания",
                render: (doc: AuctionsModel) => doc.created_date
            }
        ]
    }
    
    const inputs = [
        {
            label:"Номер заявки содержит",
            type: "text"
        }, 
        {
            label:"Код лота содержит",
            type: "text"
        }, 
        {
            label:"Стартовая цена содержит",
            type: "text"
        }, 
        {
            label:"Заказчик",
            type: "select",
            options:['ТОО "KAZ Minerals Aktogay"', 'АО "АК Алтыналмас"']
        }, 
        {
            label:"Published",
            type: "select",
            options:["Неизвестно", "Да", "Нет"]
        }, 
        {
            label:"Подача заявок до",
            type: "date"
        }, 
        {
            label:"Дата проведения",
            type: "date"
        }
    ];
    const buttonsTitle = [{name:'обновить биржу'}];

    const actions = {edit:false, view: true, delete: true}

    return (
      <BasePage actions={actions} fetchRows={fetchRows} columns={calcCol()} headerLabel="аукционов" title="Аукционы" inputs={inputs} buttonsTitle={buttonsTitle}/>     
    );
}