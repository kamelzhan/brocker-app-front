import { FC, useEffect, useState } from "react";
import { BasePageDetails } from "../../../BasePage/BasePageDetails/BasePageDetails";
import { AuctionDetailsProps } from "./props";
import { useParams } from "react-router-dom";
import axios from "axios";
import { AuctionsModel } from "../Models/AuctionsModel";
import { Details, DetailsRows } from "../../../BasePage/BasePageDetails/props";

export const AuctionDetails:FC<AuctionDetailsProps> = () => {
    
    let {id, isEdit} = useParams();

    const returnColumns = () => {
        return {
            generalPart: {
                rows:[
                    {
                        columnLabel:"Название товара",
                        columnValue: "",
                        columnName:"product_name"
                    },
                    {
                        columnLabel:"Код брокера инициатора",
                        columnValue: "",
                        columnName:"broker_code"
                    },
                    {
                        columnLabel:"Вид аукциона",
                        columnValue: "",
                        columnName:"auction_kind"
                    },
                    {
                        columnLabel:"Размер ГО",
                        columnValue: "",
                        columnName:"exchange_collateral_amount"
                    },
                    {
                        columnLabel:"Внесение обеспечения до",
                        columnValue: "",
                        columnName:"deposit_date"
                    },
                    {
                        columnLabel:"Инициатор",
                        columnValue: "",
                        columnName:"initiator"
                    },
                    {
                        columnLabel:"Секция торговли",
                        columnValue: "",
                        columnName:"trade_section"
                    },
                    {
                        columnLabel:"Направление аукциона",
                        columnValue: "",
                        columnName:"auction_direction"
                    },
                    {
                        columnLabel:"Размер ГО факт",
                        columnValue: "",
                        columnName:"fact_exchange_collateral_amount"
                    },
                    {
                        columnLabel:"Дата проведения",
                        columnValue: "",
                        columnName:"trade_date"
                    },
                    {
                        columnLabel:"Updated date",
                        columnValue: "",
                        columnName:"updated_date"
                    },
                    {
                        columnLabel:"Номер заявки",
                        columnValue: "",
                        columnName:"request_number"
                    },
                    {
                        columnLabel:"Номер лота",
                        columnValue: "",
                        columnName:"lot_num"
                    },
                    {
                        columnLabel:"Тип аукциона",
                        columnValue: "",
                        columnName:"auction_type"
                    },
                    {
                        columnLabel:"Минимальные требование",
                        columnValue: "",
                        columnName:"minimum_requirements"
                    },
                    {
                        columnLabel:"Подача заявок",
                        columnValue: "",
                        columnName:"applying_time"
                    },
                    {
                        columnLabel:"Комментарии",
                        columnValue: "",
                        columnName:"comments"
                    },
                    {
                        columnLabel:"Заказчик",
                        columnValue: "",
                        columnName:"customer"
                    },
                    {
                        columnLabel:"Код лота",
                        columnValue: "",
                        columnName:"lot_code"
                    },
                    {
                        columnLabel:"Стартовая цена",
                        columnValue: "",
                        columnName:"starting_price"
                    },
                    {
                        columnLabel:"Подача заявок до",
                        columnValue: "",
                        columnName:"applying_date"
                    },
                    {
                        columnLabel:"Заключение сделок",
                        columnValue: "",
                        columnName:"transaction_time"
                    }
                ], 
                title:"Основные данные"
            }
        }
    }

    const returnRows = () => {
        const data = returnColumns();
        axios.get(`/ets/auctions/${id}`)
            .then((res)=> {
                data.generalPart.rows = data.generalPart.rows.map((item)=>{
                    if(item.columnName === "customer"){
                        return {
                            columnLabel: item.columnLabel,
                            columnName: item.columnName,
                            columnValue: res.data.customer.name
                        }
                    }
                    return {
                        columnLabel: item.columnLabel,
                        columnName: item.columnName,
                        columnValue: res.data[item.columnName]    
                    }
                })
                setRows(data);
        })
    }
    const[rows, setRows] = useState<DetailsRows>(returnColumns());
    useEffect(()=>{
        returnRows();
    },[])
    
    // useEffect(()=>{
    //     if(!isCreate){
    //         axios.get(`/ets/auctions/${id}`)
    //         .then((res)=> {
    //             const detailsRows = returnRows();
    //             detailsRows.generalPart.rows = detailsRows.generalPart.rows.map((item)=>{
    //                 if(item.columnName === "customer"){
    //                     return {
    //                         columnLabel: item.columnLabel,
    //                         columnName: item.columnName,
    //                         columnValue: res.data.customer.name
    //                     }
    //                 }
    //                 return {
    //                     columnLabel: item.columnLabel,
    //                     columnName: item.columnName,
    //                     columnValue: res.data[item.columnName]    
    //                 }
    //             })
    //             setRows(detailsRows)
    //         })
    //     }
    //     else{
    //         setRows(returnRows());
    //     }
    // },[])

    return(
        <BasePageDetails path={`auctions`} data={rows} />
    );
}