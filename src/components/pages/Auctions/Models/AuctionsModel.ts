import { CustomerModel } from "../../Customers/Models/CustomerModel";

export interface AuctionsModel {
    id: number;
    applying_date:string;
    applying_time: string;
    auction_direction:string;
    auction_kind:string;
    auction_type:string;
    broker_code:string;
    comments:string;
    contenders_list_file:string;
    created_date:string;
    customer: CustomerModel;
    deposit_date:string;
    exchange_collateral_amount:string;
    fact_exchange_collateral_amount:string;
    initiator:string;
    lot_code:string;
    lot_num:string;
    minimum_requirements:string;
    product_name:string;
    published:string;
    qualification_file:string;
    request_number:string;
    starting_price:string;
    trade_date:string;
    trade_section:string;
    trading_rules_file:string;
    transaction_time:string;
    updated_date:string;

}