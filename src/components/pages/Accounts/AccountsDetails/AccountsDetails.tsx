import { FC, useEffect, useState } from "react";
import { BasePageDetails } from "../../../BasePage/BasePageDetails/BasePageDetails";
import { useParams } from "react-router-dom";
import { Details, DetailsRows } from "../../../BasePage/BasePageDetails/props";
import axios from "axios";
import { AccountsModel } from "../Models/AccountsModel";

export const AccountsDetails :FC = () => {

    
    let {id} = useParams();

    const returnColumns = () => {
        return {
            generalPart:{
                rows: [
                    {
                        columnLabel:"ID",
                        columnValue: "",
                        columnName:"id"
                    },
                    {
                        columnLabel:"Наименование клиента",
                        columnValue: "",
                        columnName:"clientName"
                    },
                    {
                        columnLabel:"ИИН/БИН",
                        columnValue: "",
                        columnName:"IIN"
                    },
                    {
                        columnLabel:"Код торгового счета",
                        columnValue: "",
                        columnName:"account_code"
                    },
                    {
                        columnLabel:"Код раздела учета регистра ГО",
                        columnValue: "",
                        columnName:"register_code"
                    },
                    {
                        columnLabel:"Код раздела регистра учета денег для оплаты товара",
                        columnValue: "",
                        columnName:"payment_code"
                    },
                    {
                        columnLabel:"Дата открытия регистра (подачи заявки)",
                        columnValue: "",
                        columnName:"open_date"
                    },
                    {
                        columnLabel:"Дата закрытия регистра",
                        columnValue: "",
                        columnName:"close_date"
                    },
                    {
                        columnLabel:"Комментарии",
                        columnValue: "",
                        columnName:"comments"
                    },
                    {
                        columnLabel:"Заявка С01",
                        columnValue: "",
                        columnName:"c01_file"
                    }
                ],
                title:"info"
            }
        }
    }

    const returnRows = () => {
        const newRows = returnColumns();
        axios.get(`/ets/accounts/${id}`)
        .then((res)=>{
            newRows.generalPart.rows = newRows.generalPart.rows.map((item)=>{
                return{
                    columnLabel: item.columnLabel,
                    columnName: item.columnName,
                    columnValue: res.data[item.columnName]
                }
            })
            setRows(newRows);
        });
    }

    const[rows, setRows] = useState<DetailsRows>(returnColumns());

    useEffect(()=>{
        returnRows();
    }, [])

    return (
        <BasePageDetails path={`accounts`} data={rows}/>
    )
}