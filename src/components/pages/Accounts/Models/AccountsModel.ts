export interface AccountsModel{
    id: string;
    clientName: string;
    IIN: string;
    account_code:string;
    register_code: string;
    payment_code: string;
    open_date: string;
    close_date: string;
    comments: string;
    c01_file: string;
}