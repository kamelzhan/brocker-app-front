import axios from "axios"
import { BasePage } from "../../BasePage/BasePage"
import { AccountsModel } from "./Models/AccountsModel"

export const Accounts = () => {
    
    const fetchRows = () => {
        return axios.get<AccountsModel[]>('ets/accounts');
    }

    const calcCol = () => {
        return [
            {
                label: "ID",
                render: (doc: AccountsModel) => doc.id
            },
            {
                label: "Наименование клиента",
                render: (doc: AccountsModel) => doc.clientName
            },
            {
                label: "ИИН/БИН",
                render: (doc: AccountsModel) => doc.IIN
            },
            {
                label: "Код торгового счета",
                render: (doc: AccountsModel) => doc.account_code
            },
            {
                label: "Код раздела учета регистра ГО",
                render: (doc: AccountsModel) => doc.register_code
            },
            {
                label: "Код раздела регистра учета денег для оплаты товара",
                render: (doc: AccountsModel) => doc.payment_code
            },
            {
                label: "Дата открытия регистра (подачи заявки)",
                render: (doc: AccountsModel) => doc.open_date
            },
            {
                label: "Дата закрытия регистра",
                render: (doc: AccountsModel) => doc.close_date
            }
        ]
    }

    const inputs = [
        {
            label:"Код торгового счета содержит",
            type: "text"
        }, 
        {
            label:"Наименование клинета",
            type: "text"
        }, 
        {
            label:"ИИН/БИН",
            type:"text"
        }, 
        {
            label:"Дата открытия регистра (подачи заявки) содержит",
            type: "date"
        }, 
        {
            label:"Дата закрытия регистра содержит",
            type: "date"
        }
    ];
    const buttonsTitle = [{name:"Импортировать счета"}];

    return(
        <BasePage title="Счета" headerLabel="заявок" fetchRows={fetchRows} columns={calcCol()} inputs={inputs} buttonsTitle={buttonsTitle} path="accounts"/>
    )
}