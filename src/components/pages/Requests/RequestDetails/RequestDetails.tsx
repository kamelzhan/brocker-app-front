import { FC, useEffect, useState } from "react";
import { BasePageDetails } from "../../../BasePage/BasePageDetails/BasePageDetails";
import { Details, DetailsRows } from "../../../BasePage/BasePageDetails/props";
import { useParams } from "react-router-dom";
import axios from "axios";

export const RequestDetails: FC = () => {

    
    let {id, isEdit} = useParams();
    const isCreate = isEdit === "create" ? true : false;

    const returnColumns = () => {
        return {
            generalPart: {
                rows: [
                    {
                        columnLabel:"ID",
                        columnValue: "",
                        columnName:"id"
                    },
                    {
                        columnLabel:"Дата заявки",
                        columnValue: "",
                        columnName:"application_date"
                    },
                    {
                        columnLabel:"Дата торгов",
                        columnValue: "",
                        columnName:"trading_date"
                    },
                    {
                        columnLabel:"Сумма поставщика",
                        columnValue: "",
                        columnName:"supplier_price"
                    },
                    {
                        columnLabel:"Номер сделки",
                        columnValue: "",
                        columnName:"deal_id"
                    },
                    {
                        columnLabel:"Дата создания",
                        columnValue: "",
                        columnName:"created_date"
                    },
                    {
                        columnLabel:"Дата обновления",
                        columnValue: "",
                        columnName:"updated_date"
                    },
                    {
                        columnLabel:"Комментарии",
                        columnValue: "",
                        columnName:"comments"
                    },
                    {
                        columnLabel:"Аукцион",
                        columnValue: "",
                        columnName:"auction"
                    },
                    {
                        columnLabel:"Поставщик",
                        columnValue: "",
                        columnName:"supplier"
                    },
                    {
                        columnLabel:"Статус",
                        columnValue: "",
                        columnName:"status"
                    },
                    {
                        columnLabel:"Тип торгов",
                        columnValue: "",
                        columnName:"lot_type"
                    }
                ],
                title: "Info"
            }
        }
    }

    const returnRows = () => {
        const newRows = returnColumns();
        axios.get(`/ets/requests/${id}`)
        .then((res)=>{
            newRows.generalPart.rows = newRows.generalPart.rows.map((item)=>{
                if (item.columnName === "auction"){
                    return {
                        columnLabel: item.columnLabel,
                        columnName: item.columnName,
                        columnValue: res.data.auction.product_name
                    }
                }
                if (item.columnName === "supplier"){
                    return {
                        columnLabel: item.columnLabel,
                        columnName: item.columnName,
                        columnValue: res.data.supplier.client_name
                    }
                }
                return{
                    columnLabel: item.columnLabel,
                    columnName: item.columnName,
                    columnValue: res.data[item.columnName]
                }
            })
            setRows(newRows);
        })
    }

    const[rows, setRows] = useState<DetailsRows>(returnColumns());

    useEffect(()=>{
        returnRows();
    },[])

    return(
        <BasePageDetails path={`requests`} data={rows} />
    )
}