import { AuctionsModel } from "../../Auctions/Models/AuctionsModel";
import { ClientsModel } from "../../Clients/Models/ClientsModel";

export interface RequestModel{
    id: string;
    deal_id: string;
    customer: string;
    productName: string;
    application_date: string;
    trading_date: string;
    startPrice: number;
    supplier: ClientsModel;
    contractNumber: string;
    clientCode: string;
    status: string;
    type: string;
    supplier_price: string;
    dealNumber: string;
    file1: string;
    file2: string;
    file3: string;
    file4: string;
    file5: string;
    file6: string;
    file7: string;
    file8: string;
    created_date: string;
    updated_date: string;
    comments: string;
    auction: AuctionsModel;
    lot_type: string;

}