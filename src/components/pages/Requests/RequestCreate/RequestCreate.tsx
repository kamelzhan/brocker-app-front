import { FC, useState } from "react";
import { RowsDetails } from "../../../BasePage/BasePageCreate/BasePageCreateProps";
import axios from "axios";
import { BasePageCreate } from "../../../BasePage/BasePageCreate/BasePageCreate";
import { ClientsModel } from "../../Clients/Models/ClientsModel";
import { CustomerModel } from "../../Customers/Models/CustomerModel";

export const RequestCreate: FC = () => {

    const getAuctions = () => {
        const data: any[] = [];
        axios.get('/ets/auctions/short')
        .then((res) => {
            const newData = res.data.map((item: any) => {
                if (item.request_number){
                    return {label:`${item.request_number} - ${item.customer}`}
                }
            })
            data.push(...newData.filter((item: any)=> Boolean(item)))
        })
        return data;
    }

    const getClients = () => {
        const data :any[] = [];
        axios.get("/ets/clients")
        .then((res)=>{
            data.push(...res.data.map((item: ClientsModel)=>{return item.client_name}))
        })
        return data;
    }

    const getCustomers = () => {
        const data: any[] = [];
        axios.get("/ets/customers")
        .then((res)=>{
            data.push(...res.data.map((item: CustomerModel)=>{return item.name}))
        })
        return data;
    }

    const returnRows = (): RowsDetails[] => {
        return [
            {
                label: "Заявка",
                name: "auction",
                inputType: "autocomplete",
                options: getAuctions()
            },
            {
                label: "Поставщик",
                name: "supplier",
                inputType: "select",
                options: getClients()
            },
            {
                label: "Код поставщика",
                name: "code",
                inputType: "text"
            },
            {
                label: "Дата подачи",
                name: "application_date",
                inputType: "date"
            },
            {
                label: "Дата торгов",
                name: "trading_date",
                inputType: "date"
            },
            {
                label: "Заказчик",
                name: "customer",
                inputType: "autocomplete",
                options: getCustomers()
            }
        ]
    }

    const [rows, setRows] = useState<RowsDetails[]>(returnRows());


    

    return(
        <BasePageCreate rows={rows} path="requests"/>
    )
}