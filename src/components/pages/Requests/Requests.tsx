import axios from "axios"
import { BasePage } from "../../BasePage/BasePage"
import { RequestModel } from "./models/RequestModel"



export const Requests = () => {

    const fetchRows = () => {
        return axios.get<RequestModel[]>('ets/requests');
    }

    const calcCol = () => {
        return [
            {
                label: "ID",
                render: (doc: RequestModel) => doc.id
            },
            {
                label: "Номер заявки",
                render: (doc: RequestModel) => doc.auction.request_number
            },
            {
                label: "Заказчик",
                render: (doc: RequestModel) => doc.auction.customer.name
            },
            {
                label: "Название товара",
                render: (doc: RequestModel) => doc.auction.product_name
            },
            {
                label: "Дата заявки",
                render: (doc: RequestModel) => doc.auction.applying_date
            },
            {
                label: "Дата торгов",
                render: (doc: RequestModel) => doc.auction.trade_date
            },
            {
                label: "Стартовая цена",
                render: (doc: RequestModel) => doc.auction.starting_price
            },
            {
                label: "Поставщик",
                render: (doc: RequestModel) => doc.supplier.client_name
            },
            {
                label: "Номер договора",
                render: (doc: RequestModel) => doc.supplier.contract_number
            },
            {
                label: "Код клиента",
                render: (doc: RequestModel) => doc.clientCode
            },
            {
                label: "Статус",
                render: (doc: RequestModel) => doc.status
            },
            {
                label: "Тип лота",
                render: (doc: RequestModel) => "no"
            },
            {
                label: "Сумма поставщика",
                render: (doc: RequestModel) => doc.supplier_price
            },
            {
                label: "Номер сделки",
                render: (doc: RequestModel) => doc.deal_id
            }
        ]
    }

    const buttonsTitle = [{name:"Новая заявка", link:true}, {name:"Импортировать заявки"}]
    const inputs = [
        {
            label:"Аукцион Номер заявки содержит",
            type: "text"
        }, 
        {
            label:"Дата заявки",
            type: "date"
        }, 
        {
            label:"Дата торгов",
            type: "date"
        }, 
        {
            label:"Статус",
            type: "text"
        }, 
        {
            label:"Поставщик Наименование клиента содержит",
            type: "text"
        }, 
        {
            label:"Сумма поставщика содержит",
            type: "text"
        }]

    return(
        <BasePage fetchRows={fetchRows} columns={calcCol()} buttonsTitle={buttonsTitle} title={"Заявки"} headerLabel="заявок" inputs={inputs} path="requests" />
    )
    
}