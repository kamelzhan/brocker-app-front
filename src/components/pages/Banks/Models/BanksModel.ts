export interface BanksModel{
    id: string;
    name: string;
    bik: string;
}