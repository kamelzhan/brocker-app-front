import { FC, useEffect, useState } from "react";
import { BasePageDetails } from "../../../BasePage/BasePageDetails/BasePageDetails";
import { Details, DetailsRows } from "../../../BasePage/BasePageDetails/props";
import { useParams } from "react-router-dom";
import axios from "axios";
import { RowsDetails } from "../../../BasePage/BasePageCreate/BasePageCreateProps";

export const BanksDetails: FC = () => {
    let {id, isEdit} = useParams();
    const isCreate = isEdit === "create" ? true : false;
    
    const returnRows = () => {
        const newRows = returnColumns();
        axios.get(`/ets/banks/${id}`)
        .then((res)=>{
            newRows.generalPart.rows = newRows.generalPart.rows.map((item)=>{
                return {
                    columnLabel: item.columnLabel,
                    columnName: item.columnName,
                    columnValue: res.data[item.columnName]
                }
            })
            setRows(newRows);
        })
    }

    const returnColumns = () => {
        return {
            generalPart:{
                rows: [
                {
                    columnLabel:"ID",
                    columnValue: "",
                    columnName:"id"
                },
                {
                    columnLabel:"Наименование банка",
                    columnValue: "",
                    columnName:"name"
                },
                {
                    columnLabel:"БИК",
                    columnValue: "",
                    columnName:"bik"
                }
            ],
            title: "Инфо"}
        }
    }
    useEffect(()=>{
        returnRows();
    }, [])

    const[rows, setRows] = useState<DetailsRows>(returnColumns());

    return(
        <BasePageDetails path={`banks`} data={rows} />
    )
}