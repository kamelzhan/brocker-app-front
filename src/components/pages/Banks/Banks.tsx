import axios from "axios"
import { BasePage } from "../../BasePage/BasePage"
import { BanksModel } from "./Models/BanksModel"

export const Banks = () => {
    
    const fetchRows = () => {
        return axios.get<BanksModel[]>('ets/banks');
    }

    const calcCol = () => {
        return [
            {
                label: "ID",
                render: (doc: BanksModel) => doc.id
            },
            {
                label: "Наименование банка",
                render: (doc: BanksModel) => doc.name
            },
            {
                label: "БИК",
                render: (doc: BanksModel) => doc.bik
            },

        ]
    }
    
    const buttonsTitle = [{name:"Добавить банк", link:true}, {name:"Импортировать банк"}];

    return (
        <BasePage title="Банки" fetchRows={fetchRows} columns={calcCol()} isFilterActive={false} buttonsTitle={buttonsTitle} path="banks"/>
    )
}