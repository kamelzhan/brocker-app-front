import { FC, useEffect, useState } from "react";
import { BasePageDetails } from "../../../BasePage/BasePageDetails/BasePageDetails";
import { Details, DetailsRows } from "../../../BasePage/BasePageDetails/props";
import { useParams } from "react-router-dom";
import axios from "axios";

export const ClientsDetails: FC = () => {
    
    
    let {id, isEdit} = useParams();

    const returnColumns = () => {
        return {
            generalPart: {
                rows: [
                    {
                        columnLabel:"ID",
                        columnValue: "",
                        columnName:"id"
                    },
                    {
                        columnLabel:"Наименование клиента",
                        columnValue: "",
                        columnName:"client_name"
                    },
                    {
                        columnLabel:"БИН/ИИН",
                        columnValue: "",
                        columnName:"identification_number"
                    },
                    {
                        columnLabel:"Номер договора",
                        columnValue: "",
                        columnName:"contract_number"
                    },
                    {
                        columnLabel:"Дата договора",
                        columnValue: "",
                        columnName:"contract_date"
                    },
                    {
                        columnLabel:"Юридический адрес",
                        columnValue: "",
                        columnName:"legal_address"
                    },
                    {
                        columnLabel:"Фактический адрес",
                        columnValue: "",
                        columnName:"fact_address"
                    },
                    {
                        columnLabel:"Р/с",
                        columnValue: "",
                        columnName:"checking_account"
                    },
                    {
                        columnLabel:"File1",
                        columnValue: "",
                        columnName:"file1"
                    },
                    {
                        columnLabel:"File2",
                        columnValue: "",
                        columnName:"file2"
                    },
                    {
                        columnLabel:"File3",
                        columnValue: "",
                        columnName:"file3"
                    },
                    {
                        columnLabel:"Дата создания",
                        columnValue: "",
                        columnName:"created_date"
                    },
                    {
                        columnLabel:"Дата обновления",
                        columnValue: "",
                        columnName:"updated_date"
                    },
                    {
                        columnLabel:"Комментарии",
                        columnValue: "",
                        columnName:"comments"
                    },
                    {
                        columnLabel:"Телефон клиента",
                        columnValue: "",
                        columnName:"phone_number"
                    },
                    {
                        columnLabel:"E-mail клиента",
                        columnValue: "",
                        columnName:"email"
                    },
                    {
                        columnLabel:"Веб-сайт клиента",
                        columnValue: "",
                        columnName:"website"
                    },
                    {
                        columnLabel:"Счет",
                        columnValue: "",
                        columnName:"account"
                    },
                    {
                        columnLabel:"Статус договора",
                        columnValue: "",
                        columnName:"contract_status"
                    },
                    {
                        columnLabel:"Тип ЭАВР",
                        columnValue: "",
                        columnName:"certificate_type"
                    },
                    {
                        columnLabel:"Банк",
                        columnValue: "",
                        columnName:"bank"
                    }
                ],
                title: "Info"
            }
        }
    }

    const returnRows = () => {
        const newRows = returnColumns();
        axios.get(`/ets/clients/${id}`)
        .then((res)=>{
            newRows.generalPart.rows = newRows.generalPart.rows.map((item)=>{
                return{
                    columnLabel: item.columnLabel,
                    columnName: item.columnName,
                    columnValue: res.data[item.columnName]
                }
            })
            setRows(newRows);
        })
    }

    const[rows, setRows] = useState<DetailsRows>(returnColumns());

    useEffect(()=>{
        returnRows();
        console.log(rows);
    },[])
    
    return(
        <BasePageDetails path={`clients`} data={rows} />
    )
}