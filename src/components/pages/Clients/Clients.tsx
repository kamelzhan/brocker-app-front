import axios from "axios"
import { BasePage } from "../../BasePage/BasePage"
import { ClientsModel } from "./Models/ClientsModel"

export const Clients = () => {

    const fetchRows = () => {
        return axios.get<ClientsModel[]>('ets/clients');
    }
    
    const calcCol = () => {
        return [
            {
                label: "ID",
                render: (doc: ClientsModel) => doc.id
            },
            {
                label: "Наименование клиента",
                render: (doc: ClientsModel) => doc.client_name
            },
            {
                label: "ИИН/БИН",
                render: (doc: ClientsModel) => doc.identification_number
            },
            {
                label: "Номер договора",
                render: (doc: ClientsModel) => doc.contract_number
            },
            {
                label: "Дата договора",
                render: (doc: ClientsModel) => doc.contract_date
            },
            {
                label: "Статус договора",
                render: (doc: ClientsModel) => doc.contract_status
            },
            {
                label: "Код клиента",
                render: (doc: ClientsModel) => "нету кода"
            },
            {
                label: "Юридический адрес",
                render: (doc: ClientsModel) => doc.legal_address
            },
            {
                label: "Фактический адрес",
                render: (doc: ClientsModel) => doc.fact_address
            },
            {
                label: "Тип ЭАВР",
                render: (doc: ClientsModel) => doc.certificate_type
            },
            {
                label: "Тип тарифа",
                render: (doc: ClientsModel) => "нет тарифа"
            },
            {
                label: "Р/с",
                render: (doc: ClientsModel) => doc.checking_account
            },
            {
                label: "Банк",
                render: (doc: ClientsModel) => doc.bank
            },
        ]
    }

    const buttonsTitle = [{name:"Новый клиент", link:true}, {name:"Импортировать клиентов"}, {name:"Скачать таблицу"}];
    const inputs = [
        {
            label:"Наименование клиента содержит",
            type: "text"
        }, 
        {
            label:"БИН/ИИН содержит",
            type: "text"
        }, 
        {
            label:"Счет Код торгового счета содержит",
            type: "text"
        }
    ];

    return(
        <BasePage fetchRows={fetchRows} columns={calcCol()} inputs={inputs} buttonsTitle={buttonsTitle} title="Клиенты" headerLabel="клиентов" path="clients"/>
    )
}