export interface ClientsModel{
    id: string;
    client_name: string;
    identification_number: string;
    contract_number: string;
    contract_date: string;
    legal_address: string;
    fact_address: string;
    checking_account: string;
    file1: string;
    file2: string;
    file3: string;
    created_date: string;
    updated_date: string;
    comments: string;
    phone_number: string;
    email: string;
    website: string;
    account: string;
    contract_status: string;
    certificate_type: string;
    bank: string;

}