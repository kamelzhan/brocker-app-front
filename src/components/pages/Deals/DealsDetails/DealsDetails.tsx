import { FC, useEffect, useState } from "react";
import { BasePageDetails } from "../../../BasePage/BasePageDetails/BasePageDetails";
import { Details, DetailsRows } from "../../../BasePage/BasePageDetails/props";
import { useParams } from "react-router-dom";
import axios from "axios";

export const DealsDetails: FC = () => {

    
    let {id, isEdit} = useParams();
    const isCreate = isEdit === "create" ? true : false;

    const returnColumns = () => {
        return {
            generalPart: {
                rows: [
                    {
                        columnLabel:"ID",
                        columnValue: "",
                        columnName:"id"
                    },
                    {
                        columnLabel:"Код брокера победителя",
                        columnValue: "",
                        columnName:"broker_code"
                    },
                    {
                        columnLabel:"Номер сделки",
                        columnValue: "",
                        columnName:"number"
                    },
                    {
                        columnLabel:"Сумма сделки",
                        columnValue: "",
                        columnName:"price"
                    },
                    {
                        columnLabel:"Дата сделки",
                        columnValue: "",
                        columnName:"created_date"
                    },
                    {
                        columnLabel:"Время сделки",
                        columnValue: "",
                        columnName:"created_time"
                    },
                    {
                        columnLabel:"Дата оплаты комиссии",
                        columnValue: "",
                        columnName:"commission_date"
                    },
                    {
                        columnLabel:"Статус комиссии",
                        columnValue: "",
                        columnName:"commission_status"
                    },
                    {
                        columnLabel:"Комментарии",
                        columnValue: "",
                        columnName:"comments"
                    },
                    {
                        columnLabel:"Заявка",
                        columnValue: "",
                        columnName:"request"
                    }
                ],
                title: "Info"
            }
        }
    }

    const returnRows = () => {
        const newRows = returnColumns();
        axios.get(`/ets/deals/${id}`)
        .then((res)=>{
            newRows.generalPart.rows = newRows.generalPart.rows.map((item)=>{
                return{
                    columnLabel: item.columnLabel,
                    columnName: item.columnName,
                    columnValue: res.data[item.columnName]
                }
            })
            setRows(newRows);
        })
    }

    const[rows, setRows] = useState<DetailsRows>(returnColumns());

    useEffect(()=>{
        returnRows();
    },[])

    return(
        <BasePageDetails path={`deals`} data={rows} />
    )
}