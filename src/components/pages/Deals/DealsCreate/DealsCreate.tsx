import { FC, useState } from "react";
import { RowsDetails } from "../../../BasePage/BasePageCreate/BasePageCreateProps";
import axios from "axios";
import { BasePageCreate } from "../../../BasePage/BasePageCreate/BasePageCreate";
import { ClientsModel } from "../../Clients/Models/ClientsModel";
import { CustomerModel } from "../../Customers/Models/CustomerModel";

export const DealsCreate: FC = () => {

    const getRequest = () => {
        const data: any[] = [];
        axios.get('/ets/auctions/short')
        .then((res) => {
            const newData = res.data.map((item: any) => {
                if (item.request_number){
                    return {label:`${item.request_number} - ${item.customer}`}
                }
            })
            data.push(...newData.filter((item: any)=> Boolean(item)))
        })
        return data;
    }

    const returnRows = (): RowsDetails[] => {
        return [
            {
                label: "Заявка",
                name: "auction",
                inputType: "autocomplete",
                options: getRequest()
            },
            {
                label: "Сумма сделки",
                name: "sum",
                inputType: "text"
            },
            {
                label: "Номер сделки",
                name: "number",
                inputType: "text"
            },
            {
                label: "Дата сделки",
                name: "deals_date",
                inputType: "date"
            },
            {
                label: "Время сделки",
                name: "deals_time",
                inputType: "text"
            }
        ]
    }

    const [rows, setRows] = useState<RowsDetails[]>(returnRows());


    

    return(
        <BasePageCreate rows={rows} path="deals"/>
    )
}