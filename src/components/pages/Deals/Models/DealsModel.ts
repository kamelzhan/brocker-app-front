import { StringLiteral } from "typescript";

export interface DealsModel {
    id: string;
    request: string;
    customer: string;
    number: string;
    client: string;
    dealNumber: string;
    created_date: string;
    commission_status: string;
    broker_code: string;
    price: string;
    created_time: string;
    commission_date: string;
    comments: string;
}