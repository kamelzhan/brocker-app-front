import axios from "axios";
import { BasePage } from "../../BasePage/BasePage";
import { DealsModel } from "./Models/DealsModel";

export const Deals = () => {

    const fetchRows = ()=>{
        return axios.get<DealsModel[]>('ets/deals');
    }

    const calcCol = () => {
        return [
            {
                label: "ID",
                render: (doc: DealsModel) => doc.id
            },
            {
                label: "Заявка",
                render: (doc: DealsModel) => doc.request
            },
            {
                label: "Заказчик",
                render: (doc: DealsModel) => "нету"
            },
            {
                label: "Номер лота",
                render: (doc: DealsModel) => "no"
            },
            {
                label: "Клиент",
                render: (doc: DealsModel) => "no"
            },
            {
                label: "Номер сделки",
                render: (doc: DealsModel) => doc.number
            },
            {
                label: "Дата сделки",
                render: (doc: DealsModel) => doc.created_date
            },
            {
                label: "Статус комиссии",
                render: (doc: DealsModel) => doc.commission_status
            }
        ]
    }

    const buttonsTitle = [{name:"Новая сделка", link:true}, {name:"Импортировать сделки"}, {name:"Скачать таблицу"}]
    const inputs = [
        {
            label:"Заявка Аукцион Номер заявки содержит",
            type:"text"
        },
        {
            label:"Заявка Поставщик Наименование клиента содержит",
            type:"text"
        },
        {
            label:"Код брокера победителя содержит",
            type:"text"
        }, 
        {
            label:"Дата оплаты комиссии",
            type:"date"
        },
        {
            label:"Статус комиссии содержит",
            type: "text"
        }
    ];

    return(
        <BasePage title="Сделки" fetchRows={fetchRows} columns={calcCol()} headerLabel="сделок" inputs={inputs} buttonsTitle={buttonsTitle} path="deals"/>
    );
}