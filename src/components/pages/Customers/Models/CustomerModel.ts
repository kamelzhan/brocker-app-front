export interface CustomerModel{
    id: string;
    name: string;
    name_short: string;
    group: string;
}