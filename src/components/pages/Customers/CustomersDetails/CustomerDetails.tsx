import { FC, useEffect, useState } from "react";
import { BasePageDetails } from "../../../BasePage/BasePageDetails/BasePageDetails";
import { Details, DetailsRows } from "../../../BasePage/BasePageDetails/props";
import { useParams } from "react-router-dom";
import axios from "axios";

export const CustomersDetails: FC = () => {

    
    let {id, isEdit} = useParams();
    const isCreate = isEdit === "create" ? true : false;

    const returnColumns = () => {
        return {
            generalPart: {
                rows: [
                    {
                        columnLabel:"ID",
                        columnValue: "",
                        columnName:"id"
                    },
                    {
                        columnLabel:"Наименование",
                        columnValue: "",
                        columnName:"name"
                    },
                    {
                        columnLabel:"Сокращенное наименование",
                        columnValue: "",
                        columnName:"name_short"
                    },
                    {
                        columnLabel:"Код группы",
                        columnValue: "",
                        columnName:"group"
                    },
                    {
                        columnLabel:"Количество файлов",
                        columnValue: "",
                        columnName: "numberOfFiles"
                    }
                ],
                title: "Info"
            }
        }
    }

    const returnRows = () => {
        const newRows = returnColumns();
        axios.get(`/ets/customers/${id}`)
        .then((res)=>{
            newRows.generalPart.rows = newRows.generalPart.rows.map((item)=>{
                return{
                    columnLabel: item.columnLabel,
                    columnName: item.columnName,
                    columnValue: res.data[item.columnName]
                }
            });
            setRows(newRows);
        })
    }

    const[rows, setRows] = useState<DetailsRows>(returnColumns());

    useEffect(()=>{
        returnRows();
    },[])

    return(
        <BasePageDetails path={`customers`} data={rows} />
    )
}