import axios from "axios"
import { BasePage } from "../../BasePage/BasePage"
import { CustomerModel } from "./Models/CustomerModel"

export const Customers = () => {
    
    const fetchRows = () => {
        return axios.get<CustomerModel[]>('ets/customers');
    }

    const calcCol = () => {
        return [
            {
                label: "ID",
                render: (doc: CustomerModel) => doc.id
            },
            {
                label: "Наименование заказчика",
                render: (doc: CustomerModel) => doc.name
            },
            {
                label: "Сокращенное наименование заказчика",
                render: (doc: CustomerModel) => doc.name_short
            },
            {
                label: "Код группы заказчика",
                render: (doc: CustomerModel) => doc.group
            }
        ]
    }

    const buttonTitles = [{name:"Новый заказчик", link:true}];

    return(
        <BasePage title="Заказчики" buttonsTitle={buttonTitles} isFilterActive={false} fetchRows={fetchRows} columns={calcCol()} path="customers"/>
    )
}