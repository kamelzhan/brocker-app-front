export interface GridState<R> {
    rows?: R[];
    count?: number;
    page: number;
    rowsPerPage: number;
}