import React from "react";
import { GridProps } from "./props";
import { styled } from '@mui/material/styles';
import { GridState } from "./state";
import {Table, TableContainer, TableBody, TableCell, TableRow, TableHead, Paper, tableCellClasses, TableFooter, TablePagination, IconButton, Menu, MenuItem} from "@mui/material";
import { TablePaginationActions } from "./Pagination/TablePaginationActions";
import "./Grid.scss";

export default class Grid<R> extends React.Component<GridProps<R>, GridState<R>>{
    constructor(props: GridProps<R>){
        super(props);

        this.state = {
            rows:[],
            count: 0,
            page: 0,
            rowsPerPage: 10,
        }
    }
    
    componentDidMount(): void {
        this.props.fetchRows()
        .then((res) =>{
            this.setState({rows:res.data, count: res.data.length})
        });
    }

    handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
      ) => {
        this.setState({page:newPage});
      };

    handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
      ) => {
        this.setState({rowsPerPage: parseInt(event.target.value, 10), page: 0})
      };

    render(): React.ReactNode {

        const StyledTableCell = styled(TableCell)(({ theme }) => ({
            [`&.${tableCellClasses.head}`]: {
              backgroundColor: "#1C3557",
              color: theme.palette.common.white,
            },
            [`&.${tableCellClasses.body}`]: {
              fontSize: 14,
            },
          }));
          
          const StyledTableRow = styled(TableRow)(({ theme }) => ({
            '&:last-child td, &:last-child th': {
              border: 0,
            },
            '&:hover':{
                backgroundColor: theme.palette.action.hover
            }
          }));

        const { columns } = this.props;
        const isEmpty = this.state.count === 0;
        return(
            <div className="Grid">
                <TableContainer component = {Paper}>
                    <Table aria-label="simple table">
                        {isEmpty && <caption>Данные в таблице отсутствуют</caption>}
                        <TableHead>
                            <StyledTableRow>
                                {columns.map((col, index) => (
                                    <StyledTableCell key={`table-head-cell-${index}`} align="center">
                                        {col.label}
                                    </StyledTableCell>
                                ))}
                            </StyledTableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.rows && this.state.rows.slice( this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                            .map((row, index) => (
                                <StyledTableRow key={`table-body-row-${index}`}>
                                    {columns.map((col, index) => (
                                        <TableCell key={`table-body-cell-${index}`}>
                                            {col.render(row)}
                                        </TableCell>
                                    ))}
                                </StyledTableRow>
                            ))
                        }
                        </TableBody>
                    </Table>
                    {!isEmpty &&
                            <TablePagination
                            rowsPerPageOptions={[10,25,50,100]}
                            colSpan={3}
                            count={this.state.count ? this.state.count : 0}
                            rowsPerPage={this.state.rowsPerPage}
                            page={this.state.page}
                            SelectProps={{
                                inputProps: {
                                    'aria-label': 'rows per page',
                                },
                                native: true
                            }}
                            onPageChange={this.handleChangePage}
                            onRowsPerPageChange={this.handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}

                        />
                            }
                </TableContainer>
               
            </div>
        );
    }
}