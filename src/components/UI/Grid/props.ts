import { AxiosResponse } from "axios";

export interface GridProps<R> {
    columns: Columns<R>[];
    fetchRows(): Promise<AxiosResponse<R[]>>;
    
}

export interface Columns<R> {
    label: string;
    width?: number;
    style?: React.CSSProperties;
    render(row: R, index?: number): React.ReactNode;
}