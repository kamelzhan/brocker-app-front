import "./Input.scss";
import {FC, useState, SyntheticEvent, ChangeEvent, useEffect} from "react";
import { Inputprops } from "./props";
import { Input, TextField, InputAdornment, IconButton,  InputLabel, FormControl, Select, Autocomplete, MenuItem, SelectChangeEvent } from "@mui/material";
import { VisibilityOff, Visibility } from "@mui/icons-material"

export const PrimaryInput: FC <Inputprops> = ({value = "", placeholder = "", type = "text", style = {}, onChange, className = "primaryInput", isValidate = true, errorMessage = "", required = false, defaultValue = "", variant = "standard", size = "medium", options = [], name}) => {
    const [selectValue, setSelectValue] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const [inputValue, setInputValue] = useState(value);
    const [newOptions, setNewOptions] = useState(options);

    useEffect(()=>{
      setNewOptions(options);
    }, [options])

    useEffect(()=>{
      setInputValue(value);
    },[value])
    

    const handleClickShowPassword = () => setShowPassword((show)=> !show)

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
      event.preventDefault();
    }

    const handleChange = (event:SelectChangeEvent)  => {
      setSelectValue(event.target.value as string);
    }

    const renderInput = () => {
      switch(type){
        case "password":
          return(
            <FormControl sx={{  width: '100%' }} variant="standard">
              <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
              <Input
                  value={inputValue}
                  id = "standard-adornment-password"
                  required = {false}
                  type = {showPassword ? "text" : "password"}
                  onChange={handleInputChange}
                  className={className}
                  endAdornment = {
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                />
             </FormControl>
          )
        case "date":
          return(
            <Input value={inputValue} size={size} type="date" onChange={handleInputChange} className={className} />
          )
        case "select":
          return (
            <FormControl fullWidth>
              <Select size={size} variant={variant} className={className} value={selectValue} label={placeholder} onChange={handleChange}>
                {newOptions.map((option) => {
                  return <MenuItem value={option}>{option}</MenuItem>
                })}
              </Select>
            </FormControl>
          )
        case "autocomplete":
          return <Autocomplete className={className} onChange={handleAutocompleteChange} options={newOptions ? newOptions : []} renderInput={(params) => <TextField {...params} label={"Выберите"}/>}/>
        default:
          return(
            <TextField
                required = {false}
                label={placeholder}
                value={inputValue}
                onChange={handleInputChange}
                className={className}
                variant={variant}
                size = {size}
            />
          )
      }
    }

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
      setInputValue(event.target.value);
      onChange && onChange(event.target.value, name as string);
    }

    const handleAutocompleteChange = (event: any, value: string | null) => {
      setInputValue(event.target.value)
      onChange && onChange(event.target.value as string, name as string);
    }

    return(
        <div>
          {renderInput()}
        </div>
    )
}