import { ChangeEvent, SyntheticEvent } from "react";

export interface Inputprops {
    type?:string;
    placeholder?:string;
    className?: string;
    style?: React.CSSProperties;
    onChange?:(value: string, name: string) => void;
    value?: string;
    errorMessage?: string;
    isValidate?: boolean;
    required?: boolean;
    defaultValue?: string;
    variant?: "filled" | "standard" | "outlined" | undefined;
    showPassword?: boolean;
    size?: "small" | "medium";
    options?: string[];
    name?: string;
}