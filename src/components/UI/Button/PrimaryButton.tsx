import { ButtonProps } from "./props";
import { Button } from "@mui/material";
import {FC} from "react";
import "./Button.scss";
export const PrimaryButton: FC<ButtonProps> = ({className = "primaryButton", onClick, disabled = false, type = "button", style, children, size = "large", variant = "contained"}) => {
    return(
        <div className="Butto">
            <Button
                onClick={onClick}
                disabled = {disabled}
                type={type}
                variant={variant}
                size = {size}
            >
                {children}
            </Button>
            
        </div>
        
    )
}