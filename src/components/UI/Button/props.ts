import { MouseEvent, ReactNode } from "react";

export interface ButtonProps {
    className?: string;
    style?: React.CSSProperties;
    disabled?: boolean;
    text?: string;
    type?: "button" | "submit" | "reset";
    onClick?: (event: MouseEvent<HTMLButtonElement>) => void;
    children: ReactNode;
    size?: "small" | "medium" | "large";
    variant?: "contained" | "text" | "outlined"
}