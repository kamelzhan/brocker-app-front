import { FC, useState } from "react";
import { PrimaryButton } from "../UI/Button/PrimaryButton";
import { PrimaryInput } from "../UI/Input/PrimaryInput";
import { LoginProps } from "./props";
import "./Login.scss";
import axios from "axios";
import { setCookie } from "../../service/cookies/cookies";
export const Login: FC<LoginProps> = ({setIsAuth}) => {

    const [state, setState] = useState({
        login:"",
        password:""
    });

    const handleLogin = () => {
        if (validate()){
            axios.post("/core/auth/",
                {
                    username: state.login,
                    password: state.password
                }, 
                {
                headers: { 
                    'Content-Type': 'application/json',
                    'Authorization':''
                }
            })
            .then(res => {
                setCookie("Token", res.data.token, {"max-age":3600});
                setIsAuth();
            })
            .catch(err => console.log(JSON.parse(err)));
        }
    }

    const validate = () => {
        if (state.login && state.password){
            return true;
        }
        return false;
        
    }

    const handleLoginChange = (value:string) => {
        setState({...state, login:value});
    }

    const handlePasswordChange = (value: string) => {
        setState({...state, password: value});
    }

        return(
            <div className="Login">
                <div className = "LoginContainer">
                    <div className="LoginHeader">
                        <h1>Login</h1>
                    </div>
                    <div className="LoginBody">
                        <PrimaryInput onChange={handleLoginChange} className="input-login" placeholder="Login" type="text"/>
                        <PrimaryInput onChange={handlePasswordChange} className="input-login" placeholder="Password" type="password"/>
                    </div>
                    <div className="LoginFoter">    
                            <PrimaryButton className="button-login" size="large" onClick={handleLogin}>
                                Login
                            </PrimaryButton>
                    </div>
                </div>
            </div>
        )
}