import React from "react";
import {Route, Router, Routes} from "react-router-dom";
import { Auctions } from "./components/pages/Auctions/Auctions";
import { Requests } from "./components/pages/Requests/Requests";
import { Deals } from "./components/pages/Deals/Deals";
import { Clients } from "./components/pages/Clients/Clients";
import { Accounts } from "./components/pages/Accounts/Accounts";
import { Banks } from "./components/pages/Banks/Banks";
import { Customers } from "./components/pages/Customers/Customers";
import { AuctionDetails } from "./components/pages/Auctions/AuctionDetails/AuctionDetails";
import { DealsDetails } from "./components/pages/Deals/DealsDetails/DealsDetails";
import { RequestDetails } from "./components/pages/Requests/RequestDetails/RequestDetails";
import { ClientsDetails } from "./components/pages/Clients/ClientsDetails/ClientsDetails";
import { AccountsDetails } from "./components/pages/Accounts/AccountsDetails/AccountsDetails";
import { BanksDetails } from "./components/pages/Banks/BanksDetails/BanksDetails";
import { CustomersDetails } from "./components/pages/Customers/CustomersDetails/CustomerDetails";
import { RequestCreate } from "./components/pages/Requests/RequestCreate/RequestCreate";
import { DealsCreate } from "./components/pages/Deals/DealsCreate/DealsCreate";


export const AppRoutes = () => {
    return (
        <div>
            <Routes>
                <Route path="/auctions" Component={Auctions}/>
                <Route path="/auctions/:id/:isEdit" Component={AuctionDetails}/>
                <Route path="/requests" Component={Requests}/>
                <Route path="/requests/create" Component={RequestCreate}/>
                <Route path="/requests/:id/:isEdit" Component={RequestDetails} />
                <Route path="/deals" Component={Deals} />
                <Route path="/deals/create" Component={DealsCreate} />
                <Route path="/deals/:id/:isEdit" Component={DealsDetails} />
                <Route path="/clients" Component={Clients}/>
                <Route path="/clients/:id/:isEdit" Component={ClientsDetails} />
                <Route path="/accounts" Component={Accounts}/>
                <Route path="/accounts/:id/:isEdit" Component={AccountsDetails} />
                <Route path="/banks" Component={Banks}/>
                <Route path="/banks/:id/:isEdit" Component={BanksDetails} />
                <Route path="/customers" Component={Customers}/>
                <Route path="/customers/:id/:isEdit" Component={CustomersDetails} />
            </Routes>

        </div>
    )
}

