import axios from "axios";
import { createContext, useEffect } from "react";
import { getCookie } from "../service/cookies/cookies";


export const authContext = createContext({});


export const AuthProvider = ({children}:any) => {

    return(
        <authContext.Provider value={{}}>
            {children}
        </authContext.Provider>
    )

}