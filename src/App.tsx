import React, { useEffect, useState } from 'react';
import './App.scss';
import {Login} from './components/Login/Login';
import { Header } from './components/Header/Header';
import Grid from './components/UI/Grid/Grid';
import { Auctions } from './components/pages/Auctions/Auctions';
import { Filter } from './components/Filter/Filter';
import { BasePage } from './components/BasePage/BasePage';
import { BasePageDetails } from './components/BasePage/BasePageDetails/BasePageDetails';
import { AppRoutes } from './AppRoutes';
import { getCookie } from './service/cookies/cookies';
import { AuctionDetails } from './components/pages/Auctions/AuctionDetails/AuctionDetails';
import axios from 'axios';


function App() {

  const [isAuth, setIsAuth] = useState(false);

  useEffect(()=>{
    if(getCookie("Token")){
      setIsAuth(true);
      axios.defaults.headers.common['Authorization'] = `Token ${getCookie('Token')}`
    }
  },[isAuth])
  
  return (
    <div className="App">
      {isAuth 
        ?
        <div>
            <Header setIsAuth={() => setIsAuth(false)}/>
            <AppRoutes/>
        </div>
        
        : <Login setIsAuth={()=>setIsAuth(true)}/>
      
      }
    </div>
  );
}

export default App;
